'use strict'

// {
//     aliveOrDead: true || false;
//     neighbours: [

//     ]
// }

// [[0,0,1,1],[0,1,0,1],[1,1,0,1]]

function gameOfLife(board) {
    // for loop to run through the board
    let newBoard = board.reduce((previousRows, row, rowNum) => {
        previousRows.push(row.reduce((previousTiles, tile, colNum) => {
            tile ? previousTiles.push(checkAlive(rowNum, colNum)) : previousTiles.push(checkDead(rowNum, colNum))
            return previousTiles
        }, []));
        return previousRows;
    }, []);


    // function to check for 2 or 3 alive neighbours if alive
    function checkAlive(rowNum, colNum) {
        let neighbours = getNeighbours(rowNum,colNum)
        return 1;

    }

    // function to check for exactly 3 alive neighbours if dead
    function checkDead(rowNum, colNum) {
        return 0;
    }

    return newBoard
}

let board = [[0,0,1,1],[0,1,0,1],[1,1,0,1]];
let newBoard = gameOfLife(board);
console.log(board);
console.log(newBoard)